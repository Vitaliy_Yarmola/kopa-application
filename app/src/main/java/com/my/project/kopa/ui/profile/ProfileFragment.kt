package com.my.project.kopa.ui.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.facebook.login.LoginManager
import com.google.firebase.auth.FirebaseAuth
import com.my.project.kopa.R
import com.my.project.kopa.databinding.FragmentProfileBinding
import com.my.project.kopa.ui.auth.LoginActivity
import com.my.project.kopa.util.FirestoreUtil
import com.my.project.kopa.util.StorageUtil

class ProfileFragment : Fragment() {
    companion object {
        private const val RC_SELECT_IMAGE = 223
    }

    private var bindingView: FragmentProfileBinding? = null
    private val binding get() = bindingView!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        bindingView = FragmentProfileBinding.inflate(inflater, container, false)

        binding.apply {
            logOut.setOnClickListener {
                FirebaseAuth.getInstance().signOut()
                LoginManager.getInstance().logOut()
                val intent = Intent(activity, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
            addImageProfile.setOnClickListener {
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = "image/*"
                startActivityForResult(intent, RC_SELECT_IMAGE)
            }
        }
        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SELECT_IMAGE && resultCode == Activity.RESULT_OK && data != null) {
            Log.d("ProfileFragment", "Photo was selected")
            val selectedPhotoUri = data.data
            selectedPhotoUri?.let {
                StorageUtil.uploadProfilePhoto(it) { imagePath ->
                    FirestoreUtil.updateUserPhoto(imagePath)
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        FirestoreUtil.getCurrentUser { user ->
            if (this@ProfileFragment.isVisible) {
                binding.nameLabel.text = user.userName
                binding.cityValue.text = user.userCity
                binding.textPhone.text = user.userNumber
                if (user.userImage != null) {
                    Glide
                        .with(this)
                        .load(user.userImage)
                        .placeholder(R.drawable.shoes)
                        .into(binding.imageProfile)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        bindingView = null
    }

}