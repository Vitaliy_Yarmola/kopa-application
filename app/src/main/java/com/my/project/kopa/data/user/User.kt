package com.my.project.kopa.data.user

data class User(val userName: String,
                val  userCity: String,
                val userNumber: String,
                val userImage: String?) {
        constructor() : this("","","",null)
}