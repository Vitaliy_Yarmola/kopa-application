package com.my.project.kopa.util

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.my.project.kopa.data.shoes.Shoes
import com.my.project.kopa.data.user.User
import java.util.*
import kotlin.collections.ArrayList

object FirestoreUtil {
    private val firestoreInstance: FirebaseFirestore by lazy { FirebaseFirestore.getInstance() }

    private val currentUserDocRef: DocumentReference
        get() = firestoreInstance.document(
            "users/${
                FirebaseAuth.getInstance().currentUser?.uid
                    ?: throw NullPointerException("UID is null.")
            }"
        )

    fun initCurrentUserIfFirstTime(onComplete: (userExist: Boolean) -> Unit) {
        currentUserDocRef.get().addOnSuccessListener { documentSnapshot ->
            if (!documentSnapshot.exists()) {
                val currentUser = FirebaseAuth.getInstance().currentUser
                val newUser = User(
                    currentUser?.displayName ?: "",
                    "", currentUser?.phoneNumber ?: "", currentUser?.photoUrl?.toString()
                )
                currentUserDocRef.set(newUser).addOnSuccessListener {
                    onComplete(false)
                }
            } else
                onComplete(true)
        }
    }

    fun updateCurrentUser(
        name: String = "",
        city: String = "",
        number: String = "",
    ) {
        val userFieldMap = mutableMapOf<String, Any>()
        if (name.isNotBlank()) userFieldMap["userName"] = name
        if (city.isNotBlank()) userFieldMap["userCity"] = city
        if (number.isNotBlank()) userFieldMap["userNumber"] = number
        currentUserDocRef.update(userFieldMap)
    }

    fun updateUserPhoto(
        profilePicturePath: String? = null,
    ) {
        val userFieldMap = mutableMapOf<String, Any>()
        if (profilePicturePath != null)
            userFieldMap["userImage"] = profilePicturePath
        currentUserDocRef.update(userFieldMap)
    }

    fun getCurrentUser(onComplete: (User) -> Unit) {
        currentUserDocRef.get()
            .addOnSuccessListener {
                onComplete(it.toObject(User::class.java)!!)
            }
    }

    fun addShoesToDatabase(
        shoesName: String,
        shoesPrice: Double,
        shoesSize: Int,
        shoesSizeType: String,
        shoesLength: Double,
        shoesWidth: Double,
        shoesMaterials: String,
        shoesDescription: String,
        shoesImages: ArrayList<String>,
        onComplete: (shoesAdded: Boolean) -> Unit,
    ) {
        val userId = FirebaseAuth.getInstance().currentUser?.uid.toString()
        val shoesDocRef = firestoreInstance.document("shoes/${UUID.randomUUID()}")
        shoesDocRef.get().addOnSuccessListener { documentSnapshot ->
            if (!documentSnapshot.exists()) {
                val newShoes = Shoes(userId,
                    shoesName,
                    shoesPrice,
                    shoesSize,
                    shoesSizeType,
                    shoesLength,
                    shoesWidth,
                    shoesMaterials,
                    shoesDescription,
                    shoesImages)
                shoesDocRef.set(newShoes).addOnSuccessListener {
                    onComplete(true)
                }
            } else onComplete(false)
        }
    }
}