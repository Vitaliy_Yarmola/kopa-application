package com.my.project.kopa.data.shoes

data class Shoes(
    val userId: String,
    val shoesName: String,
    val shoesPrice: Double,
    val shoesSize: Int,
    val shoesSizeType: String,
    val shoesLength: Double,
    val shoesWidth: Double,
    val shoesMaterials: String,
    val shoesDescription: String,
    val shoesImages: ArrayList<String>
) {
    constructor() : this("","", 0.0, 0,
        "", 0.0, 0.0, "", "", arrayListOf())
}