package com.my.project.kopa.ui.loading

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import com.my.project.kopa.R

class LoadingDialog(myActivity: Activity) {
    private var activity: Activity? = myActivity
    private lateinit var dialog: AlertDialog

    @SuppressLint("InflateParams")
    fun startLoadingDialog(){
        val builder = AlertDialog.Builder(activity)
        val inflater = activity?.layoutInflater
        builder.setView(inflater?.inflate(R.layout.custom_dialog, null))
        builder.setCancelable(false)
        dialog = builder.create()
        dialog.show()
    }

    fun dismissDialog(){
        dialog.dismiss()
    }
}