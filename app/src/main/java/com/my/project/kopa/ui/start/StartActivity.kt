package com.my.project.kopa.ui.start

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import com.my.project.kopa.R
import com.my.project.kopa.ui.auth.DataActivity
import com.my.project.kopa.ui.auth.LoginActivity
import com.my.project.kopa.ui.main.MainActivity
import com.my.project.kopa.util.FirestoreUtil
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        var intent = Intent(this, LoginActivity::class.java)
        if (FirebaseAuth.getInstance().currentUser != null) {
            FirestoreUtil.getCurrentUser { user ->
                intent = if (user.userCity.isEmpty())
                    Intent(this, DataActivity::class.java)
                else Intent(this, MainActivity::class.java)
            }
        }

        GlobalScope.launch {
            delay(2000)
            startActivity(intent)
            finish()
        }
    }
}