package com.my.project.kopa.ui.product

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.my.project.kopa.R

class AboutProductActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_product)
    }
}