package com.my.project.kopa.util

import android.net.Uri
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.util.*
import kotlin.collections.ArrayList

object StorageUtil {
    private val storageInstance: FirebaseStorage by lazy { FirebaseStorage.getInstance() }

    private val currentUserRef: StorageReference
        get() = storageInstance.reference
            .child(FirebaseAuth.getInstance().currentUser?.uid
                ?: throw NullPointerException("UID is null."))

    fun uploadProfilePhoto(
        imageUri: Uri,
        onSuccess: (imagePath: String) -> Unit
    ) {
        val ref = currentUserRef.child("profilePictures/${UUID.randomUUID()}")
        ref.putFile(imageUri)
            .addOnSuccessListener {snapshot ->
                Log.d("StorageUtil", "Successfully uploaded image: ${snapshot.metadata?.path}")
                ref.downloadUrl.addOnSuccessListener {
                    Log.d("StorageUtil", "File location: $it")
                    onSuccess(it.toString())
                }
            }
    }

    fun uploadShoesPhoto(
        images: ArrayList<Uri>,
        onSuccess: (imagesPath: ArrayList<String>) -> Unit
    ) {
        val imagesPath = ArrayList<String>()
        images.forEach { image ->
            val ref = currentUserRef.child("shoes/${UUID.randomUUID()}")
            ref.putFile(image)
                .addOnSuccessListener { snapshot ->
                    Log.d("StorageUtil", "Successfully uploaded image: ${snapshot.metadata?.path}")
                    ref.downloadUrl.addOnSuccessListener {
                        Log.d("StorageUtil", "File location: $it")
                        imagesPath.add(it.toString())

                    }
                }
        }
        onSuccess(imagesPath)
    }

}