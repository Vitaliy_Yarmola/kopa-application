package com.my.project.kopa.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.my.project.kopa.R
import com.my.project.kopa.ui.advertisement.AddNewAdvertisement

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val bottomNavViewInflater = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        val navController = findNavController(R.id.fragmentNav)
        val addNewShoes = findViewById<ImageButton>(R.id.addNewShoes)

        bottomNavViewInflater.setupWithNavController(navController)
        bottomNavViewInflater.setOnNavigationItemReselectedListener {}
        addNewShoes.setOnClickListener {
            val intent = Intent(this, AddNewAdvertisement::class.java)
            startActivity(intent)
        }
    }
}