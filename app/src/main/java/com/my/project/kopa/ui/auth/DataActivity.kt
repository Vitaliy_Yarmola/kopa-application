package com.my.project.kopa.ui.auth

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.marginEnd
import androidx.core.view.marginStart
import androidx.core.view.marginTop
import androidx.core.widget.addTextChangedListener
import com.google.android.material.textfield.TextInputLayout
import com.my.project.kopa.databinding.ActivityDataBinding
import com.my.project.kopa.ui.main.MainActivity
import com.my.project.kopa.util.FirestoreUtil

class DataActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityDataBinding.inflate(layoutInflater)
        FirestoreUtil.getCurrentUser { user ->
            if (user.userName.isNotEmpty()) {
                val fullName = user.userName.split(" ")
                binding.editTextName.editText?.setText(fullName[0])
                binding.editTextSurname.editText?.setText(fullName[1])
            }
            if (user.userNumber.isNotEmpty()) {
                binding.editTextNumber.visibility = View.INVISIBLE
                binding.editTextNumber.editText?.setText(user.userNumber)
            }
        }
        binding.apply {
            val textList = listOf(editTextName, editTextSurname, editTextCity, editTextNumber)
            for (field in textList)
                field.editText?.addTextChangedListener(
                    afterTextChanged = {
                        checkEditTextForCorrectData(field, it)
                    })

            buttonApply.setOnClickListener {
                var checkSuccess = true
                for (field in textList) {
                    checkEditTextForCorrectData(field, null)
                }
                for (field in textList) {
                    if(field.error!=null){
                        checkSuccess = false
                        break
                    }
                }

                if (checkSuccess) {
                    val name = editTextName.editText?.text.toString().trim()
                    val surname = editTextSurname.editText?.text.toString().trim()
                    val city = editTextCity.editText?.text.toString().trim()
                    val number = editTextNumber.editText?.text.toString().trim()
                    FirestoreUtil.updateCurrentUser("$name $surname", city, number)
                    val intent = Intent(this@DataActivity, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                }
            }
        }

        setContentView(binding.root)
    }

    private fun checkEditTextForCorrectData(field: TextInputLayout, it: Editable?) {
        val regexStr = "^[+][0-9]{12}$".toRegex()
        val prefix = "+380"
        val params = field.layoutParams as ViewGroup.MarginLayoutParams
        val text = it?.toString() ?: field.editText?.text.toString()
        if (field.editText?.inputType == InputType.TYPE_CLASS_NUMBER && !text
                .matches(regexStr)
        ) {
            if (!text.startsWith(prefix)) {
                field.editText?.setText(prefix)
                field.editText?.setSelection(4)
            }
            params.setMargins(field.marginStart, field.marginTop, field.marginEnd, 25)
            field.error = "Номер введено невірно"
        } else if (field.editText?.inputType != InputType.TYPE_CLASS_NUMBER && (text
                .isEmpty() || checkForDigits(text))
        ) {
            params.setMargins(field.marginStart, field.marginTop, field.marginEnd, 25)
            field.error = "Поле не повинне бути порожнім або містити цифри"
        } else {
            params.setMargins(field.marginStart, field.marginTop, field.marginEnd, 0)
            field.error = null
        }
        field.layoutParams = params
    }

    private fun checkForDigits(str: CharSequence): Boolean {
        var cp: Int
        var i = 0
        while (i < str.length) {
            cp = Character.codePointAt(str, i)
            if (Character.isDigit(cp)) {
                return true
            }
            i += Character.charCount(cp)
        }
        return false
    }
}