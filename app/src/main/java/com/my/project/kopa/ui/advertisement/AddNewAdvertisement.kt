package com.my.project.kopa.ui.advertisement

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.util.Log
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.core.view.marginEnd
import androidx.core.view.marginStart
import androidx.core.view.marginTop
import androidx.core.widget.addTextChangedListener
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.my.project.kopa.R
import com.my.project.kopa.databinding.ActivityAddNewAdvertisementBinding
import com.my.project.kopa.util.FirestoreUtil
import com.my.project.kopa.util.StorageUtil

class AddNewAdvertisement : AppCompatActivity() {
    companion object {
        private const val RC_SELECT_IMAGE = 264
    }

    private var images: ArrayList<Uri>? = null
    private var shoesImagesList: List<ImageView>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityAddNewAdvertisementBinding.inflate(layoutInflater)
        shoesImagesList = listOf(binding.shoesImage1,
            binding.shoesImage2,
            binding.shoesImage3,
            binding.shoesImage4,
            binding.shoesImage5,
            binding.shoesImage6,
            binding.shoesImage7,
            binding.shoesImage8)
        binding.apply {
            val list = listOf(modelText, materialText, descriptionText, costText)
            for (field in list)
                field.editText?.addTextChangedListener(
                    afterTextChanged = {
                        checkFieldsForCorrectData(field, it)
                    })
            exitButton.setOnClickListener {
                onBackPressed()
            }
            saveNewShoes.setOnClickListener {
                var checkSuccess = true
                for (field in list) {
                    checkFieldsForCorrectData(field, null)
                }
                for (field in list) {
                    if (field.error != null) {
                        checkSuccess = false
                        break
                    }
                }
                if (images!!.isEmpty()) {
                    checkSuccess = false
                    Toast.makeText(applicationContext,
                        "Додайте хоча б одне фото",
                        Toast.LENGTH_LONG).show()
                }
                if (checkSuccess) {
                    val size = btnSize.text.toString()
                    val format = btnFormat.text.toString()
                    val height = btnHeight.text.toString()
                    val width = btnWidth.text.toString()
                    val model = modelText.editText?.text.toString().trim()
                    val material = materialText.editText?.text.toString().trim()
                    val description = descriptionText.editText?.text.toString().trim()
                    val cost = costText.editText?.text.toString().trim()
                    StorageUtil.uploadShoesPhoto(images!!) { firestorePath ->
                        FirestoreUtil.addShoesToDatabase(model,
                            cost.toDouble(),
                            size.toInt(),
                            format,
                            height.toDouble(),
                            width.toDouble(),
                            material,
                            description,
                            firestorePath) {
                            if (it) Toast.makeText(applicationContext,
                                "Об'ява успішно добавлена",
                                Toast.LENGTH_SHORT).show()
                        }
                    }

                }
            }
        }
        images = ArrayList(8)
        seActionOnImageClick(binding.shoesImage1)
        setContentView(binding.root)
    }

    private fun seActionOnImageClick(image: ImageView) {
        image.setOnClickListener {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
                val intent = Intent()
                intent.type = "image/*"
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                intent.action = Intent.ACTION_PICK
                startActivityForResult(intent,
                    RC_SELECT_IMAGE)
            }
        }
    }

    private fun removeAllImages() {
        images?.clear()
        shoesImagesList!![0].setImageResource(R.drawable.ic_camera)
        for (i in 1 until 8)
            shoesImagesList!![i].setImageResource(R.drawable.ic_no_image)
    }

    private fun checkFieldsForCorrectData(layout: TextInputLayout, it: Editable?) {
        val text = it?.toString() ?: layout.editText?.text.toString()
        if (layout.editText?.inputType == InputType.TYPE_CLASS_TEXT && text.isEmpty()) {
            layout.error = "Поле не повинне бути порожнім"
        } else if (layout.editText?.inputType == InputType.TYPE_NUMBER_FLAG_SIGNED && (text.isEmpty() || text.toDouble() < 0)) {
            layout.error = "Введіть коректну ціну"
        } else layout.error = null
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SELECT_IMAGE && resultCode == Activity.RESULT_OK && data != null) {
            Log.d("AddNewAdvertisement", "Photo was selected")
            removeAllImages()
            if (data.clipData != null) {
                val count = if (data.clipData!!.itemCount > 8) 8 else data.clipData!!.itemCount
                for (i in 0 until count) {
                    images!!.add(data.clipData!!.getItemAt(i).uri)
                }
                for (index in 0 until images!!.size) {
                    shoesImagesList!![index].let {
                        Glide
                            .with(this)
                            .load(images!![index])
                            .into(it)
                    }
                }
            } else {
                shoesImagesList!![0].let {
                    Glide
                        .with(this)
                        .load(data.data)
                        .into(it)
                }
            }
        }
    }
}